package com.example.blogposts.controler;

import com.example.blogposts.adapter.PostAdapter;
import com.example.blogposts.exception.PostException;
import com.example.blogposts.model.PostRequest;
import com.example.blogposts.model.PostResponse;
import com.example.blogposts.service.PostService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/posts")
@Api(description = "Operations with posts")
public class PostController {

    private final PostService postService;
    private final PostAdapter postAdapter;

    @GetMapping
    @ResponseStatus(OK)
    public List<PostResponse> getFeedOfPosts() {
        return postAdapter.convert((postService.findFeedOfPosts()));
    }

    @GetMapping(params = "author")
    @ResponseStatus(OK)
    public List<PostResponse> findPostsByAuthor(@RequestParam String author,
                                                @RequestParam(defaultValue = "0") int page,
                                                @RequestParam(defaultValue = "10") int size,
                                                @RequestParam(defaultValue = "date") String sortBy,
                                                @RequestParam(defaultValue = "ASC") String sortDirection) {
        return postAdapter.convert(postService
                .findByAuthor(author, page, size, sortBy, Sort.Direction.fromString(sortDirection)));
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public PostResponse createPost(@Valid @RequestBody PostRequest postRequest) {
        return postAdapter.convert(postService.save(postAdapter.convert(postRequest)));
    }

    @DeleteMapping("/{id}")
    @ApiResponses(
            {@ApiResponse(code = 204, message = "Successfully deleting")})
    @ResponseStatus(NO_CONTENT)
    public void deletePost(@PathVariable Long id) throws PostException {
        postService.deleteById(id);
    }

    @PutMapping("/{id}")
    @ResponseStatus(OK)
    public void updatePost(@PathVariable Long id,
                           @RequestBody PostRequest postRequest) {
        postService.updateById(id, postAdapter.convert(postRequest));
    }
}
