package com.example.blogposts.controler;

import com.example.blogposts.adapter.CommentAdapter;
import com.example.blogposts.exception.PostException;
import com.example.blogposts.model.CommentNewRequest;
import com.example.blogposts.model.CommentResponse;
import com.example.blogposts.model.CommentUpdateRequest;
import com.example.blogposts.service.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/posts/{postId}/comments")
public class CommentController {

    private final CommentService commentService;
    private final CommentAdapter commentAdapter;

    @GetMapping
    @ResponseStatus(OK)
    public List<CommentResponse> getCommentsOfPost(@PathVariable Long postId) {
        return commentAdapter.convert(commentService.getCommentsOfPost(postId));
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public CommentResponse createComment(@PathVariable Long postId, @Valid @RequestBody CommentNewRequest commentNewRequest) {
        return commentAdapter.convert(commentService.createComment(commentAdapter.convert(commentNewRequest, postId)));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteComment(@PathVariable Long id, @PathVariable Long postId) throws PostException {
        commentService.deleteComment(id, postId);
    }

    @PutMapping("/{id}")
    @ResponseStatus(OK)
    public CommentResponse editComment(@PathVariable Long id, @PathVariable Long postId,
                                       @RequestBody CommentUpdateRequest commentUpdateRequest) {
        return commentAdapter.convert(commentService.updateComment(id, postId, commentAdapter.convert(commentUpdateRequest)));
    }

}
