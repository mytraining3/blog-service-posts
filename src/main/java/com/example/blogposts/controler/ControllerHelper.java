package com.example.blogposts.controler;

import com.example.blogposts.exception.PostException;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Collections;
import java.util.Map;


@ControllerAdvice
public class ControllerHelper {

    @ExceptionHandler(PostException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public String handlerError(PostException noSuchElementException){
        return noSuchElementException.getMessage();
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public String handlerError(Exception e){
        return e.getMessage();
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public Map<String, String> notFoundHandler(ChangeSetPersister.NotFoundException e){
        return Collections.singletonMap("message", e.getMessage());
    }
}
