package com.example.blogposts.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
@RequiredArgsConstructor
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    private static final String[] SWAGGER_ENDPOINTS = {"/swagger-ui.html*", "/webjars/**", "/api/**", "/v2/**", "/swagger-resources/**"};
    private static final String[] PERMITTED_ENDPOINTS = {"/oauth/token"};
    private static final String ACTUATOR_ENDPOINT = "/actuator/**";

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(SWAGGER_ENDPOINTS).permitAll()
                .antMatchers(PERMITTED_ENDPOINTS).permitAll()
                .antMatchers(ACTUATOR_ENDPOINT).permitAll()
                .anyRequest().authenticated();
    }
}
