package com.example.blogposts.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.web.util.DefaultUriBuilderFactory;

@Configuration
public class ClientConfig {

    @Bean
    @Qualifier("userServiceTemplate")
    public OAuth2RestTemplate restTemplate(OAuth2ProtectedResourceDetails resource,
                                           @Qualifier("oauth2ClientContext") OAuth2ClientContext context,
                                           @Value("${blog.userservice.url}") String serviceUrl) {
        OAuth2RestTemplate oAuth2RestTemplate = new OAuth2RestTemplate(resource, context);
        oAuth2RestTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(serviceUrl));
        return oAuth2RestTemplate;
    }
}
