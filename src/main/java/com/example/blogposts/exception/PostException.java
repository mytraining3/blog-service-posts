package com.example.blogposts.exception;

public class PostException extends RuntimeException {

    public PostException(String s){
        super(s);
    }
}
