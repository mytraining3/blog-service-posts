package com.example.blogposts.client.entity;

import lombok.Data;

import java.util.Date;

@Data
public class UserResponse {

    private Long id;
    private String username;
    private String fullName;
    private Date lastSeen;
}
