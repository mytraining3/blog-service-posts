package com.example.blogposts.client.entity;

import lombok.Data;

@Data
public class SubscriptionResponse {

    private Long id;
    private UserResponse target;
    private UserResponse source;
}
