package com.example.blogposts.client;

import com.example.blogposts.client.entity.SubscriptionResponse;
import com.example.blogposts.service.CurrentUserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@AllArgsConstructor
public class UserServiceClient {

    private static final String GET_SUBSCRIPTIONS_INFO_ENDPOINT = "/subscriptions?source={username}";

    @Qualifier("userServiceTemplate")
    private final RestTemplate restTemplate;

    private final CurrentUserService currentUserService;

    public List<SubscriptionResponse> getListOfCurrentUserSubscriptions() {
        return restTemplate
                .exchange(GET_SUBSCRIPTIONS_INFO_ENDPOINT,
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<List<SubscriptionResponse>>() {},
                        currentUserService.getUsernameOfCurrentUser())
                .getBody();
    }
}

