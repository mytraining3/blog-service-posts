package com.example.blogposts.adapter;

import com.example.blogposts.model.Comment;
import com.example.blogposts.model.CommentNewRequest;
import com.example.blogposts.model.CommentResponse;
import com.example.blogposts.model.CommentUpdateRequest;
import com.example.blogposts.model.map.CommentMapper;
import com.example.blogposts.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
@RequiredArgsConstructor
public class CommentAdapter {

    private final CommentMapper commentMapper;
    private final AuthorService authorService;

    public CommentResponse convert(Comment comment) {
        return commentMapper.map(comment);
    }

    public Comment convert(CommentNewRequest commentNewRequest, Long postId) {
        Comment comment = commentMapper.map(commentNewRequest);
        comment.setPostId(postId);
        comment.setAuthor(authorService.getCurrentAuthor());
        return comment;
    }

    public Comment convert(CommentUpdateRequest commentUpdateRequest){
        return commentMapper.map(commentUpdateRequest);
    }

    public List<CommentResponse> convert(List<Comment> comments) {
        return comments.stream().map(commentMapper::map).collect(toList());
    }
}
