package com.example.blogposts.adapter;

import com.example.blogposts.model.Post;
import com.example.blogposts.model.PostRequest;
import com.example.blogposts.model.PostResponse;
import com.example.blogposts.model.map.PostMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class PostAdapter {

    private final PostMapper postMapper;

    public Post convert(PostRequest postRequest) {
        return postMapper.map(postRequest);
    }

    public PostResponse convert(Post post) {
        return postMapper.map(post);
    }

    public List<PostResponse> convert(List<Post> posts) {
        return posts.stream().map(postMapper::map).collect(Collectors.toList());
    }
}
