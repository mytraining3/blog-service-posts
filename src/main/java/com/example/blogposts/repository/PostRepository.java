package com.example.blogposts.repository;

import com.example.blogposts.model.Post;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PostRepository extends MongoRepository<Post, Long> {

    List<Post> findByAuthor_Username(String s, Pageable pageable);

    List<Post> findPostByAuthor_UsernameIn(List<String> authorsUsernamesList);
}
