package com.example.blogposts.repository;

import com.example.blogposts.model.Comment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends MongoRepository<Comment, Long> {

    List<Comment> findAllByPostId(Long postId);
}
