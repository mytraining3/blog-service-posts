package com.example.blogposts.model;

import lombok.Data;

import java.util.Date;

@Data
public class CommentResponse {
    private Long id;
    private String author;
    private String content;
    private Date date;
}
