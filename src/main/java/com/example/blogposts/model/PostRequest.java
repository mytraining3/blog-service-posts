package com.example.blogposts.model;

import lombok.Data;

@Data
public class PostRequest {

    private String topic;
    private String name;
    private String text;

}
