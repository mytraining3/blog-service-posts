package com.example.blogposts.model;

import lombok.Data;

import java.util.Date;

@Data
public class PostResponse {

    private Long id;
    private String author;
    private Date date;
    private String topic;
    private String name;
    private String text;


}
