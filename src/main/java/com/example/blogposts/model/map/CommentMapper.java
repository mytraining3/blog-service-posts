package com.example.blogposts.model.map;

import com.example.blogposts.model.Comment;
import com.example.blogposts.model.CommentNewRequest;
import com.example.blogposts.model.CommentResponse;
import com.example.blogposts.model.CommentUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Date;

@Mapper(componentModel = "spring", imports = Date.class)
public abstract class CommentMapper {

    @Mapping(target = "date", expression = "java(new Date())")
    public abstract Comment map(CommentNewRequest commentNewRequest);

    @Mapping(target = "date", expression = "java(new Date())")
    public abstract Comment map(CommentUpdateRequest commentUpdateRequest);

    @Mapping(target = "author", source = "author.username")
    public abstract CommentResponse map(Comment comment);
}
