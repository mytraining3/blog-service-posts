package com.example.blogposts.model.map;

import com.example.blogposts.model.Post;
import com.example.blogposts.model.PostRequest;
import com.example.blogposts.model.PostResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Date;

@Mapper(componentModel = "spring", imports = Date.class)
public abstract class PostMapper {

    @Mapping(target = "date", expression = "java(new Date())")
    public abstract Post map(PostRequest postRequest);

    @Mapping(target = "author", source = "author.username")
    public abstract PostResponse map(Post post);
}
