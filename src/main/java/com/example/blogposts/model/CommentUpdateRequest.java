package com.example.blogposts.model;

import lombok.Data;

@Data
public class CommentUpdateRequest {
    private String content;
}
