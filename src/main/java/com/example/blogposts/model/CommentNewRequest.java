package com.example.blogposts.model;

import lombok.Data;

@Data
public class CommentNewRequest {
    private String content;
}
