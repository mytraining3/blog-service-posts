package com.example.blogposts.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document(collection = "comments")
public class Comment {

    @Id
    private Long id;
    private Long postId;
    private Author author;
    private String content;
    private Date date;
}
