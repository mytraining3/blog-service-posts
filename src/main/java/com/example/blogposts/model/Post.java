package com.example.blogposts.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document(collection = "post")
public class Post {

    @Id
    private Long id;
    private String topic;
    private String name;
    private String text;
    private Date date;
    private Author author;

}
