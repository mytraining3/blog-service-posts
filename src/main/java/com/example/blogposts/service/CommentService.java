package com.example.blogposts.service;

import com.example.blogposts.exception.PostException;
import com.example.blogposts.model.Author;
import com.example.blogposts.model.Comment;
import com.example.blogposts.repository.CommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CommentService {

    private final CommentRepository commentRepository;
    private final AuthorService authorService;

    public List<Comment> getCommentsOfPost(Long postId) {
        return commentRepository.findAllByPostId(postId);
    }

    public Comment createComment(Comment comment) {
        return commentRepository.save(comment);
    }

    public void deleteComment(Long id, Long postId) {
        Comment comment = commentRepository
                .findById(id).orElseThrow(() -> new PostException("this comment doesn't exist"));
        if (!doesAuthorOfCommentMatchesWithCurrentAuthor(comment.getAuthor())) {
            throw new RuntimeException(); // todo BLOG-7
        }
        if (!comment.getPostId().equals(postId)) {
            throw new RuntimeException(); // todo BLOG-7
        }
        commentRepository.delete(comment);
    }

    public Comment updateComment(Long id, Long postId, Comment newComment) {
        Comment comment = commentRepository
                .findById(id).orElseThrow(() -> new PostException("this comment doesn't exist"));
        comment.setContent(newComment.getContent());
        if (!doesAuthorOfCommentMatchesWithCurrentAuthor(comment.getAuthor())) {
            throw new RuntimeException();// todo BLOG-7
        }
        if (!comment.getPostId().equals(postId)) {
            throw new RuntimeException();// todo BLOG-7
        }
        return commentRepository.save(comment);

    }

    private boolean doesAuthorOfCommentMatchesWithCurrentAuthor(Author author) {
        return author.getUsername().equals(authorService.getCurrentAuthor().getUsername());
    }
}
