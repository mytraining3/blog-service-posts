package com.example.blogposts.service;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class CurrentUserService {

    public String getUsernameOfCurrentUser() {
        return SecurityContextHolder.getContextHolderStrategy().getContext().getAuthentication().getName();
    }
}
