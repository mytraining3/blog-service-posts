package com.example.blogposts.service;

import com.example.blogposts.client.UserServiceClient;
import com.example.blogposts.client.entity.SubscriptionResponse;
import com.example.blogposts.client.entity.UserResponse;
import com.example.blogposts.exception.PostException;
import com.example.blogposts.model.Author;
import com.example.blogposts.model.Post;
import com.example.blogposts.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class PostService {

    private final PostRepository postRepository;
    private final AuthorService authorService;
    private final UserServiceClient userServiceClient;
    private final CurrentUserService currentUserService;

    private final Random random = new Random();

    public List<Post> findFeedOfPosts() {
        List<String> listOfUsernames = userServiceClient.getListOfCurrentUserSubscriptions()
                .stream()
                .map(SubscriptionResponse::getTarget)
                .map(UserResponse::getUsername)
                .collect(toList());
        return postRepository.findPostByAuthor_UsernameIn(listOfUsernames);
    }

    public List<Post> findByAuthor(String author, int page, int size, String sortBy, Sort.Direction sortDirection) {
        return postRepository.findByAuthor_Username(author, PageRequest.of(page, size, Sort.by(sortDirection, sortBy)));
    }

    public Post save(Post post) {
        post.setId(random.nextLong());
        post.setAuthor(authorService.getCurrentAuthor());
        return postRepository.save(post);
    }

    public void deleteById(Long id) {
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new PostException("there are no posts with this id"));
        if (doesAuthorMatchesWithCurrentUser(post.getAuthor())) {
            postRepository.deleteById(id);
        } else {
            throw new PostException("User cant delete another post");
        }
    }

    public void updateById(Long id, Post postWithNewParameters) {
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new PostException("there are no post with id " + id));
        Author authorOfPost = post.getAuthor();
        if (doesAuthorMatchesWithCurrentUser(authorOfPost)) {
            postWithNewParameters.setId(post.getId());
            postWithNewParameters.setAuthor(authorOfPost);
            postRepository.save(postWithNewParameters);
        } else {
            throw new PostException("you are not author of this post");
        }
    }

    private boolean doesAuthorMatchesWithCurrentUser(Author author) {
        return author.getUsername().equals(currentUserService.getUsernameOfCurrentUser());
    }
}
