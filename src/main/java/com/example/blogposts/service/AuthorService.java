package com.example.blogposts.service;

import com.example.blogposts.model.Author;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthorService {

    private final CurrentUserService currentUserService;

    public Author getCurrentAuthor() {
        return new Author(currentUserService.getUsernameOfCurrentUser());
    }
}
