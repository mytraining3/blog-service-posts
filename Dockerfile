FROM adoptopenjdk/openjdk15:alpine-jre

ADD target/blogposts-service-*-SNAPSHOT.jar /blogposts-service.jar

ENV JAVA_OPTS = $JAVA_OPTS

CMD ["sh", "-c", "java ${JAVA_OPTS} -jar /blogposts-service.jar"]

EXPOSE 8080